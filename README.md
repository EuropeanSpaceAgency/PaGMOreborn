PaGMO
=====

[![build status](https://gitlab.com/EuropeanSpaceAgency/PaGMOreborn/badges/master/build.svg)](https://gitlab.com/EuropeanSpaceAgency/PaGMOreborn/commits/master)
[![Build status](https://ci.appveyor.com/api/projects/status/vqnefl3o2v7acqsw?svg=true)](https://ci.appveyor.com/project/bluescarni/pagmoreborn)
[![codecov](https://codecov.io/gl/EuropeanSpaceAgency/PaGMOreborn/branch/master/graph/badge.svg)](https://codecov.io/gl/EuropeanSpaceAgency/PaGMOreborn)
