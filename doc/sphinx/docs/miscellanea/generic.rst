.. cpp_generic_utilities

Generic utilities
=================

A number of utilities to compute quantities that are of general relevance.

--------------------------------------------------------------------------

.. doxygenfunction:: uniform_real_from_range
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::decision_vector(const std::pair<vector_double, vector_double>&, detail::random_engine_type&)
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::decision_vector(const vector_double&, const vector_double&, detail::random_engine_type&)
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::safe_cast
  :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::binomial_coefficient
  :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::kNN
  :project: PaGMOreborn
