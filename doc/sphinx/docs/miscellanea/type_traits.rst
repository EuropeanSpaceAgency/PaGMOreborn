.. cpp_type_traits

Type traits
===========

Type traits used in PaGMO

.. doxygenclass:: pagmo::has_fitness
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_bounds
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_e_constraints
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_i_constraints
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_name
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_extra_info
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_gradient
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::override_has_gradient
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_gradient_sparsity
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::override_has_gradient_sparsity
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_hessians
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::override_has_hessians
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_hessians_sparsity
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::override_has_hessians_sparsity
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_set_verbosity
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::override_has_set_verbosity
   :project: PaGMOreborn
   :members:

.. doxygenclass:: pagmo::has_evolve
   :project: PaGMOreborn
   :members:
