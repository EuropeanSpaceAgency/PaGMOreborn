.. cpp_moead

Multi-objective Evolutionary Algorithm by Decomposition (MOEA/D-DE)
===================================================================

.. doxygenclass:: pagmo::moead
   :project: PaGMOreborn
   :members:
