.. cpp_multi_objective_optimization

Multi-objective optimization utilities
======================================

A number of utilities to compute quantities that are of relevance to
the determination of non dominated fronts, Pareto dominance criterias and
more in general, to multi-objective optimization tasks.

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::pareto_dominance
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::non_dominated_front_2d
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::crowding_distance
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::fast_non_dominated_sorting
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::sort_population_mo
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::select_best_N_mo
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::ideal
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::nadir
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::decomposition_weights
  :project: PaGMOreborn
